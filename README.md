# Scripts

## Bookmark manager

**A simple bookmark manager for termux terminal emulator 
on android.**

I needed a simple bookmark manager on android and I don't like to 
use sync to save my bookmarks across different devices. I created 
this script to manage bookmarks easily. Termux have few addons, 
termux-widget being one of them which let users to use shell 
scripts on android homescreen. I mainly use it with termux-widget 
and created the script to be used with termux. It has few termux 
specific dependencies which made it less portable. I might 
change that in future but for now its only usable in termux. 


## Dependencies
There are two types of dependencies with this script, one is 
android specific and the other is packages from termux repo. 
I have decided to call them external(android) and internal(termux) 
dependencies.

- Android specific: termux and it's addons installed as APKs.
- Termux packages: packages from default repo.

### External dependencies

- [`Termux terminal`](https://f-droid.org/en/packages/com.termux/)
- [`Termux-widget`](https://f-droid.org/en/packages/com.termux.widget)
- [`Termux-api`](https://f-droid.org/en/packages/com.termux.api/)

You can download the apk from F-droid and side load it.

### Internal dependencies
- [`fzf`](https://github.com/junegunn/fzf)
- [`jq`](https://github.com/stedolan/jq)
- [`termux-api`](https://github.com/termux/termux-api)
- [`git`](https://git-scm.com/)

You can install the packages using pkg in termux

```bash
$ pkg install jq fzf termux-api git
```

## Installation
### Setup
```bash
$ mkdir .shortcuts
$ git clone https://codeberg.org/upanandX11/scripts.git
$ cp scripts/bookmark_manager.sh .shortcuts/
```

#### Termux widget setup
On android add a widget for termux-widget from widget list.


