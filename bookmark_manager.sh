#!/bin/env bash

##########################################################################
# Copyright (C) 2023 Upanand Mahato
#
# This program is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation,either version 3 of the License,or 
# (at your option)any later version.
#
# This program is distributed in the hope that it will be useful,but
# WITHOUT ANY WARRANTY;without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARITCULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of GNU General Public License 
# along with this program. If not,see <https://www.gnu.org/licenses/>.#
###########################################################################

# Created on Sun Apr 23 16:28:01 IST 2023
# Description: bookmark manager script
#
 
# Bookmarks directory
file_dir=temp

# Setup and file check
# Directory check
if [[ -d $HOME/${file_dir} ]]
then :
else 
	mkdir -p $HOME/${file_dir}
fi
#
# JSON file check
if [[ -f $(find $HOME/${file_dir}/ -name '*.json' 2> /dev/null) ]]
then :
else
	touch $HOME/${file_dir}/bookmarks.json
	echo -e "{\n}" > $HOME/${file_dir}/bookmarks.json
fi
#
# Directory for json file
json_file=$(find $HOME/${file_dir}/*.json)

# Functions 

# Exit on exit code for termux-dialog
close () {
		if [[ $exit_code == 2 ]];
		then exit
		fi
	}

# Hack fix for orientation problem of termux-dialog
fix_ori () {
		while [[ $bookmark_name == "" ]]
		do
			$1
		done
	}

# fzf menu to select the option
menu () {
	selection=$(echo -e "Show bookmark\nAdd bookmark" | fzf) 
}

#---------------
#|Add bookmarks|
#---------------

get_bookmark_name () {

	main_var=$(termux-dialog text -t "Enter bookmark name")
	
	bookmark_name=$(echo $main_var | jq '.text' -r)
	
	exit_code=$(echo $main_var | jq '.code' -r | sed s/-//g)

	close
}

add_bookmark () {

	url=$(termux-clipboard-get)

	jq --arg bookmark_name "$bookmark_name" --arg url "$url" '. + {($bookmark_name): ($url)}' $json_file |\
	jq '.' > $(mktemp --suffix=.json $HOME/${file_dir}/bookmarks.XXX)

	rm $json_file

	unset bookmark_name
	unset url
}


#-----------------
#| Show bookmarks|
#-----------------

show_bookmark () {

	bookmark_name=$(jq 'keys' $json_file | jq '.[]' -r |\
			fzf --border --header="Select bookmark" --no-scrollbar)

	url=$(jq --arg bookmark_name "$bookmark_name" '.[$bookmark_name]' $json_file -r)

	termux-open-url $url
}

# fzf menu
menu
# Case statement to select appropriate option
case $selection in 
	Show*) show_bookmark ;;
	 Add*) get_bookmark_name;fix_ori get_bookmark_name;add_bookmark ;;
	    *) echo "Invalid selection"
esac
exit
